#!/bin/python
# https://towardsdatascience.com/turn-google-sheets-into-your-own-database-with-python-4aa0b4360ce7

import gspread
import os, sys, argparse, re
from oauth2client.service_account import ServiceAccountCredentials
import pandas as pd

parser = argparse.ArgumentParser(description='Top Disk Manager uploader')
parser.add_argument( '-i', '--reportname', type = str, default="report.csv", help = "Path to file list containing quotas" )
args = parser.parse_args()

SS_URL = "https://docs.google.com/spreadsheets/d/12izVLjVZ1pm15GXqOu6QeeMwx2rDx4mtkjds_YeRbcI"
KEYFILE = "secrets/topdiskmanager2022-b4f04c2b9eb6.json"

# Create DataFrame with data
inputFile = args.reportname
input = pd.read_csv(inputFile)

df = pd.DataFrame(data=input['Folder'].copy())
df['Analysis'] = ''
df['TB'] = input['Size'].str.extract(r"([0-9.]+)").astype(float) * input['Size'].str.extract(r"([A-Z]+)").replace(['T','G','M','K'],[1.0,1e-3,1e-6,1e-9]).astype(float)
df['Number of Files'] = input['Number of Files'].copy().astype(int)
df['Contact Person(s)'] = input['Contact Person(s)'].copy()

# Remove useless folders
df = df[df["Folder"].str.contains("QuotaManagement|user|quota_test") == False].iloc[:,[1,0,2,3,4]]

# Connect to Google Sheets
scope = ['https://www.googleapis.com/auth/spreadsheets',
         "https://www.googleapis.com/auth/drive"]

credentials = ServiceAccountCredentials.from_json_keyfile_name(KEYFILE, scope)
client = gspread.authorize(credentials)

# Open the spreadsheet
sheet = client.open_by_url(SS_URL).sheet1


# Get the grouped folder information
groupedFolders = sheet.get('G7:J14')
result = []
for i in groupedFolders:
    folder_list = []
    try:
        folders = i[1].split(',')
    except:
        print("WARNING:  couldn't split.")
        folders = i[1]
    for entry in folders:
        if '*' in entry:
            entry = entry.replace('*','.*')
            folder_list.extend(df.loc[df['Folder'].str.contains(entry),'Folder'].tolist())

        folder_list.append(entry)

    df_case_folder = df[df['Folder'].isin(folder_list)].groupby('Analysis')['Folder'].agg(lambda x: '+'.join(x.sort_values().unique())).reset_index()
    df_case_TB =     df[df['Folder'].isin(folder_list)].groupby('Analysis')['TB'].agg('sum').reset_index()
    df_case_number =     df[df['Folder'].isin(folder_list)].groupby('Analysis')['Number of Files'].agg('sum').reset_index()
    df_case_people =     df[df['Folder'].isin(folder_list)].groupby('Analysis')['Contact Person(s)'].agg('sum').reset_index()
    
    df_final = df_case_folder.merge(df_case_TB)
    df_final = df_final.merge(df_case_number)
    df_final = df_final.merge(df_case_people)
    df_final['Analysis'] = df_final['Analysis'].replace('', i[0])
    result.append(df_final)

    # Remove merged options from main df
    df = df[df['Folder'].isin(folder_list) == False]

result.append(df)
result = pd.concat(result)

summary_filepath = os.getenv('outfile')
summary = []
cells_to_update = ['G19', 'G20', 'G21', 'G22', 'G23']
for idx, sumline in enumerate(pd.read_csv(summary_filepath, chunksize=1, header=None, encoding='utf-8')):
    sheet.update( cells_to_update[idx], sumline.iloc[0,0])


#print(type(sheet))
sheet.batch_clear(['A2:C1000'])
sheet.update([result.columns.values.tolist()] + result.values.tolist())


