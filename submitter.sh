#!/bin/bash

export LOCAL_TOPDISKMANAGER_PATH=`pwd`
date=$(printf '%(%Y-%m-%d)T\n' -1)
export here=$(realpath ".")

if [ -d "$here/condor/log" ]; then
  ### Take action if log exists ###
  echo "INFO: log folder ok"
else
  ###  Control will jump here if $DIR does NOT exists ###
  mkdir -p $here/condor/log
fi

if [ -d "$here/condor/out" ]; then
  ### Take action if log exists ###
  echo "INFO: out folder ok"
else
  ###  Control will jump here if $DIR does NOT exists ###
  mkdir -p $here/condor/out
fi

if [ -d "$here/condor/err" ]; then
  ### Take action if log exists ###
  echo "INFO: err folder ok"
else
  ###  Control will jump here if $DIR does NOT exists ###
  mkdir -p $here/condor/err
fi

subFile="$here/condor/log/EOS-quota-${date}.sub"
> $subFile

echo "getenv	 = True" >> $subFile
echo "Executable = $LOCAL_TOPDISKMANAGER_PATH/reader.sh" >> $subFile
echo "Error      = /afs/cern.ch/user/g/gguerrie/atlas-top-disk-manager/condor/err/EOS-quota-${date}.err" >> $subFile
echo "Output     = /afs/cern.ch/user/g/gguerrie/atlas-top-disk-manager/condor/out/EOS-quota-${date}.out" >> $subFile
echo "Log        = /afs/cern.ch/user/g/gguerrie/atlas-top-disk-manager/condor/log/EOS-quota-${date}.log" >> $subFile
echo "+JobFlavour = \"longlunch\"" >> $subFile
echo "max_retries             = 10" >> $subFile
echo "requirements = Machine =!= LastRemoteHost" >> $subFile
echo "Queue" >> $subFile

condor_submit $subFile
