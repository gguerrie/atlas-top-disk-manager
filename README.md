# ATLAS Top Disk Manager

- **[Prerequisites](#prerequisites)**
    - **[Create a new project in GCP](#create-a-new-project-in-gcp)**
    - **[Enable the APIs, create credentials, and download them](#enable-the-apis-create-credentials-and-download-them)**
- **[Getting Started](#getting-started)** 
- **[Running the workflow](#running-the-workflow)**
    - **[Enabling acron jobs](#enabling-acron-jobs)**

ToDo:
- add explanation on spreadsheet usage
- add description of files
- add description of HTCondor submissions

### Disclaimer

This repository is inspired to [this tutorial](https://towardsdatascience.com/turn-google-sheets-into-your-own-database-with-python-4aa0b4360ce7) and to the [Top Disk Manager Instructions page](https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/TopDiskManagerInstructions), so in order to have further clarifications, please feel free to explore them.

#### Workflow
This tool allows users to get the information about disk usage on lxplus and automatically upload a record on a Google spreasheet.

## Prerequisites
In order to upload information on a google spreadsheet, it is necessary to have access to the [Google Cloud Platform](https://cloud.google.com/free/) (GCP). The registration and the resources used to maintain this infrastructure allow the user to remain under the *free tier*, i.e. no charges. </br>
Within GCP, the main service used will be the [Google APIs](https://cloud.google.com/apis/docs/resources/enterprise-apis), in particular:
- [Sheets API](https://developers.google.com/sheets/api)
- [Drive](https://developers.google.com/drive/api) - (optional)

Below are listed the main steps to follow in order to get the infrastructure ready. Please follow them thoroughly.
#### Create a new project in GCP
To create a new project, go to [this website](https://console.cloud.google.com/) and follow the steps below (you need to be already logged into your Google account).
1. Select a Project
2. Click on "New Project"
3. A new page will load. There you have to write the name of your new project and click on "Create"

<div align="center">
  <img src="./docs/1.webp" />
</div>

4. Once you create your project, you'll see the image below:


<div align="center">
  <img src="./docs/2.webp" />
</div>

Click on "Select Project" You should see now a new page.


#### Enable the APIs, create credentials, and download them
After selecting the project, go to the left panel and click on "APIs & Services" and then select "Library" as shown below.

<div align="center">
  <img src="./docs/3.png" />
</div>

You should see the page below now. There we have to search Google Sheets API (and the Drive one) and enable it. Once it's enabled, go to the API dashboard and select the "Credentials" tab. Then click on "Create Credentials" and select "Service account".

<div align="center">
  <img src="./docs/4.png" />
</div>

Fill the required fields, and most importantly *choose the "Editor" role*, as shown below.

<div align="center">
  <img src="./docs/5.webp" />
</div>

In the API summary page, under the section "Service accounts", will appear the created account. Click on the account, and navigate to the "Keys" tab, then click "Add key"-->"Create new key". A JSON file with your credentials will be downloaded to your computer. This file will be needed for the next steps.

Now the most difficult part is done, congratulations!

## Getting Started

Get the repository: 
```
git clone ssh://git@gitlab.cern.ch:7999/gguerrie/atlas-top-disk-manager.git
```

### Setup via python virtual environment
At the beginning of each (each!) session, _**please execute**_ 
```
source setup.sh
```

this will set up all the necessary packages, nominally `gspread`, `oauth2client` and `pandas`. Since most of these libraries are not already included into a standard LGC stack, a python virtual environment will be created. Once the session is over, the envionment will remain stored in the `env/` folder. **To deactivate the virtual environment and get rid of it**, just execute:
```
deactivate
rm -rf env/
```

### Tuning the important paramenters
Create the service folders inside the cloned repo:
```
mkdir reports secrets
```

#### Store the secrets
Upload the JSON file with the credentials in the `secrets` folder. Moreover, since we will use rucio, please make sure that your GRID certificate is correctly set-up. In order to fully automatize the process, it can be useful to create a `password.pass` file inside the `secrets` directory. This file will contain the GRID passphrase required by the `voms` command. This is not mandatory, if for security reasons you do not want to have the passphrase written in a file, the script will ask for it (making the process more secure, but less automatized).

#### Create a spreadsheet
Create a new Google spreadsheet; it is HIGHLY recommended to copy/paste [the baseline](https://docs.google.com/spreadsheets/d/12izVLjVZ1pm15GXqOu6QeeMwx2rDx4mtkjds_YeRbcI/edit?usp=sharing). Then add as a contributor the service account created in GCP: add the email (e.g. `soap-1722@topdiskmanager2022.iam.gserviceaccount.com`) and set the permission as "Editor".

#### Setup updater file
At line `13` and `14` of the `updater.py` file, replace the strings with the parameters that you have now, i.e. the path to the `.json` file and the url of the spreadsheet.

## Running the workflow
Run the following command:

```
source reader.sh
```
This script will check the disk usage for `EOS`, and for the rucio RSEs devoted to the `phys-top` group. The running time can take quite some time, since it is necessary to asses the size of all the analyses on `EOS` (up to several TBs). After this step, the information will be stored on the Google spread sheet, and a summary of the disks usage will be saved in the `reports` folder.

### Enabling acron jobs
CERN provides a service called [acron](https://acrondocs.web.cern.ch/), that allows users to execute periodically jobs on Linux machines. In order to setup an `acron` job, verify that you acron account is correctly setup by executing:
```
man acron
```
If this works successfully, create the acron job by using `acron jobs create`. Find an example below:
```
acron jobs create -s '0 14 ? * 3,5,7' -t lxplus.cern.ch -c 'cd atlas-top-disk-manager/ && source setup.sh && source reader.sh' -d 'Top Disk Manager script'
```
Please notice that the option `-s '0 14 ? * 3,5,7'` encodes the time schedule for the execution of the job. 
This syntax mean "execute the commands at 14:00, every week on Tuesdays, Thursdays and Saturdays". You can find more information about schedule formatting at https://crontab.guru/. Notice also that the `?` sign in lxplus is meaning 'whatever value' and might be not standard in other cron job schedulers. 

For more information, please follow the instructions located at https://acrondocs.web.cern.ch/quickstart/. Please notice that as a CERN user, it's probably not necessary to follow the steps regarding user authentication etc. 
