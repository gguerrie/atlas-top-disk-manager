#!/bin/sh
# Some messages to the user
echo "************************************************************************" 
echo -e "SETUP\t Welcome, this is the TopDiskManager setup script" 
echo "" 

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase 
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh -3 --quiet
source /cvmfs/sft.cern.ch/lcg/views/LCG_103/x86_64-centos7-gcc11-opt/setup.sh

echo -e "SETUP\t Setting up environmental variables" 
if [ -f env/bin/activate ]; then
    echo -e "INFO\t Activating virtual environment.."
    source env/bin/activate 
else
    echo -e "INFO\t Creating new virtual environment.."
    python3 -m venv env
    source env/bin/activate 
    pip install --upgrade pip
    pip install gspread
    pip install oauth2client
    pip install pandas
fi

echo "************************************************************************" 
echo -e "SETUP\t Configuration finished!"
