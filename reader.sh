#!/bin/bash

if [ -z ${LOCAL_TOPDISKMANAGER_PATH+x} ]; then
  LOCAL_TOPDISKMANAGER_PATH=`pwd`
fi
source $LOCAL_TOPDISKMANAGER_PATH/setup.sh

eos_command=$(eos quota /eos/atlas/atlascerngroupdisk/phys-top) && eos_command=`echo $eos_command | sed 's/[^0-9TBM.:k ]*//g'` && IFS=':' read -ra eos_quota <<< "$eos_command" && IFS=' ' read -ra values <<< "${eos_quota[-1]}"

date=$(printf '%(%Y-%m-%d)T\n' -1)

export outfile=$LOCAL_TOPDISKMANAGER_PATH/reports/report-${date}.txt
reportOutfile=$LOCAL_TOPDISKMANAGER_PATH/report.csv

if [ -d "$LOCAL_TOPDISKMANAGER_PATH/reports" ]; then
  ### Take action if log exists ###
  echo -e "INFO\t log folder ok"
else
  ###  Control will jump here if $DIR does NOT exists ###
  mkdir -p $LOCAL_TOPDISKMANAGER_PATH/reports
fi

# Create output files
> $outfile
> $reportOutfile
echo "Size,Folder,Number of Files,Contact Person(s)" >> $reportOutfile

echo "Occupied space on EOS: ${values[3]} ${values[4]} / ${values[9]} ${values[10]} - ${values[13]}%" >> $outfile
echo "Written files on EOS: ${values[5]} ${values[6]} / ${values[11]} ${values[12]}" >> $outfile
echo "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -" >> $outfile

shopt -s dotglob
eos ls -lh /eos/atlas/atlascerngroupdisk/phys-top | grep '^d' | while IFS= read -r d; do 
    unset users
    users=""
    IFS=' ' read -ra info <<< "${d}" && folder=${info[-1]} && size=${info[-6]} && prefix=${info[-5]}
    if [ $folder == "*.txt" ];then 
      continue; 
    fi

    #Get the list of users IDs
    userlist_command=$(eos attr ls eos/atlas/atlascerngroupdisk/phys-top/$folder | grep "user.acl*") && IFS=',' read -ra userlist <<< "${userlist_command}"
    for i in ${userlist[@]}; do
      if [[ $i != *u:* ]];then
        continue;
      fi
      USER_NUMBER=$(echo $i | tr -dc '0-9')
      USER_INFOS=$(getent passwd userid $USER_NUMBER)
      if [ -z "$USER_INFOS" ];then 
        continue; 
      fi
      IFS=':' read -ra USER_TEMP <<< "${USER_INFOS}" && IFS=',' read -ra USER_NAME <<< "${USER_TEMP[4]}"
      # Fill a string with the name of the users
      if [ -z "$users" ];then 
        users="${USER_NAME[0]} (${USER_TEMP[0]})"
      else
        users="${users}, ${USER_NAME[0]} (${USER_TEMP[0]})"
      fi
    done
    #echo $users

    number=$(find /eos/atlas/atlascerngroupdisk/phys-top/$folder -type f | wc -l)
    echo -e "READER\t Checking $folder"
    echo "${size}${prefix},$folder,$number,\"$users\" " >> $reportOutfile

done

# Checking rucio RSEs
if [[ -f $LOCAL_TOPDISKMANAGER_PATH/secrets/password.pass ]]; then
    cat $LOCAL_TOPDISKMANAGER_PATH/secrets/password.pass | voms-proxy-init -voms atlas #--cert .globus/usercert.pem --key .globus/userkey.pem
else
    echo -e "WARNING\t Didn't find password.pass, you will need to provide the GRID passphrase!" 
    voms-proxy-init -voms atlas #--cert .globus/usercert.pem --key .globus/userkey.pem
fi
lsetup rucio

echo "Occupied space on EOS: ${values[3]} ${values[4]} / ${values[9]} ${values[10]} - ${values[13]}%"
echo "Written files on EOS: ${values[5]} ${values[6]} / ${values[11]} ${values[12]}"
echo "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -"

# Get info on CERN-PROD_PHYS-TOP and DESY-ZN_PHYS-TOP
cerndisk=$(rucio list-account-usage phys-top | grep CERN-PROD_PHYS-TOP) && IFS='|' read -ra CERN_quota <<< "$cerndisk"
echo "Occupied space on CERN DISK: ${CERN_quota[2]} / ${CERN_quota[3]}" >> $outfile
echo "Occupied space on CERN DISK: ${CERN_quota[2]} / ${CERN_quota[3]}"
desydisk=$(rucio list-account-usage phys-top | grep DESY-ZN_PHYS-TOP) && IFS='|' read -ra DESY_quota <<< "$desydisk"
echo "Occupied space on DESY DISK: ${DESY_quota[2]} / ${DESY_quota[3]}" >> $outfile
echo "Occupied space on DESY DISK: ${DESY_quota[2]} / ${DESY_quota[3]}"


echo -e "READER\t Uploading information" 
# Update spreadsheet
python $LOCAL_TOPDISKMANAGER_PATH/updater.py

echo -e "READER\t Finished!" 

echo -e "" 
echo -e "" 
echo -e "HELPER\t Message for the biweekly report:"
echo -e "" 

#Setting up stupid stuff to format the message for the report, cause I'm a lazy mf
percentage=$(awk "BEGIN {printf \"%.2f\", (${values[3]} / 50) * 100}")
echo "Occupied space on EOS: ${values[3]} ${values[4]} / 50 ${values[10]} - ${percentage}%"
echo "Written files on EOS: ${values[5]} ${values[6]} / ${values[11]} ${values[12]}"

DESY_value=$(echo "${DESY_quota[3]}" | tr -d '[:alpha:]' | tr -d '[:space:]')
CERN_value=$(echo "${CERN_quota[2]}" | tr -d '[:alpha:]' | tr -d '[:space:]')

if [[ ${DESY_quota[2]} == *"GB"* ]]; then
  sum=${CERN_value}
  desy_placeholder=0
elif [[ ${DESY_quota[2]} == *"TB"* ]]; then
  sum=$(bc <<< "${DESY_value} + ${CERN_value}")
  desy_placeholder=$DESY_value
fi

percentage=$(awk "BEGIN {printf \"%.2f\", ($sum / 150) * 100}")
leftover_space=$(awk "BEGIN {printf \"%.2f\", (150 - $sum)}")

echo -e "Grid: CERN $CERN_value /${CERN_quota[3]} - DESY ${desy_placeholder} /${DESY_quota[3]} (${percentage}% occupied, $leftover_space / 150 TB free)"
