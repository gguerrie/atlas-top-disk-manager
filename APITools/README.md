# ATLAS Top Disk Manager - APIs

- **[Getting Started](#getting-started)** 
- **[Getting production roles information](#getting-production-roles-information)**

## Getting Started

This tool exploits the VOMS API documentation, you can find it [here](http://italiangrid.github.io/voms/documentation/voms-admin-guide/3.8.1/api.html).\
In order to make the tool work, is only necessary to make sure that the `key.pem` file created is correctly setup.

As explained in the [support pages](https://ca.cern.ch/ca/Help/?kbid=024010), one needs to produce `.pem` files for both the certificate and the key.\
Notice that the script **won't work** unless the header of userkey.pem is set to `-----BEGIN RSA PRIVATE KEY-----`; in case you have it beginning with `-----BEGIN ENCRYPTED PRIVATE KEY-----` please run:
```
openssl rsa -in .globus/userkey.pem -out .globus/new_userkey.pem 
```
and proceed to use `new_userkey.pem` for the following operations.

### Getting production roles information
To get production roles information in the ATLAS community, firstly execute the setup in the main directory.
```
source setup.sh
```
Then get into this folder (`cd APITools`) and execute:

```
source API_caller.sh
```
**WARNING**: It takes a long time to get all the information about ATLAS physicists (~50 min!)

This script will retrieve the VOMS information about each user (in `atlas_VOMS.json`), and it will pass it to `API_processor.py` that will output the list of names having production rights on `/atlas/phys-top` (in`VOMS_namelist.txt`).
