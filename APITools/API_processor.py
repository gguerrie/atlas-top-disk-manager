import json, os
import pandas as pd
from pandas.io.json import json_normalize

with open(os.path.join(os.getcwd(),"atlas_VOMS.json")) as fd:
    json_data = json.load(fd)

df = json_normalize(json_data['result'])

bool_list = ["/atlas/phys-top/Role=production" in i for i in df['fqans'].tolist()]

certificates = df[bool_list]['certificates']

name_list = []
for idx,entry in enumerate(certificates):
        try:
            name = certificates.iloc[idx][-1]['subjectString'] # Get the latest entry for certificate, that normally has the most complete name
            if any(char.isdigit() for char in str(name.split('CN')[-1].replace("=", ""))):
                 name = certificates.iloc[idx][0]['subjectString']
        except:
            name = certificates.iloc[idx][0]['subjectString'] # Get the fist entry for certificate, that for sure exists

        name_list.append(str(name.split('CN')[-1].replace("=", ""))) # Get the CN field (containing the name) and remove the '=' sign
        print(str(name.split('CN')[-1].replace("=", "")))

with open(os.path.join(os.getcwd(),"VOMS_namelist.txt"), 'w') as fp:
    fp.write('\n'.join(name_list))
