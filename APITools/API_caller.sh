#!/bin/bash

# IMPORTANT!!!! READ BELOW

# Please make sure that you produced the correct cert files.
# Notice that curl won't work unless the header of userkey.pem is set to -----BEGIN RSA PRIVATE KEY-----
# In case you have it beginning with -----BEGIN ENCRYPTED PRIVATE KEY-----, please run:
# openssl rsa -in .globus/userkey.pem -out .globus/new_userkey.pem 
# and proceed to pass .globus/new_userkey.pem 
# -------------------------------------------------------------------------------------------------------------------

curl -H "X-VOMS-CSRF-GUARD: y" \
--capath /etc/grid-security/certificates/ \
--cert /afs/cern.ch/user/g/gguerrie/.globus/usercert.pem --key /afs/cern.ch/user/g/gguerrie/.globus/new_userkey.pem \
https://voms25.cern.ch:8443/voms/atlas/apiv2/users?pageSize=4000 > atlas_VOMS.json

python API_processor.py